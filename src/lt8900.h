/*
 * lt8900.h
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 */

#ifndef LT8900_H_
#define LT8900_H_

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include "Spi.h"
#include "Lt8900Control.h"

#define LT8900_CS_DDR DDRC
#define LT8900_CS_PORT PORTC
#define LT8900_CS_SPIN PORTC
#define LT8900_CS_PIN PC2

#define LT8900_RST_DDR DDRC
#define LT8900_RST_PORT PORTC
#define LT8900_RST_SPIN PORTC
#define LT8900_RST_PIN PC1

#define LT8900_PKT_DDR DDRC
#define LT8900_PKT_PORT PORTC
#define LT8900_PKT_SPIN PINC
#define LT8900_PKT_PIN PC0

struct buspacket_t;

#define SYNC_WORD 0x0123456789abcdef

class Lt8900
{
public:
	Lt8900(uint8_t rf_address, Spi *spi, Lt8900Control *lt8900Control, uint8_t retransmit, uint8_t channel, bool pkt_pullup);

	void init_hardware();
	void reset();
	bool test();

	void setSyncWord(uint64_t syncword);
	void setAddress(uint8_t address);
	uint8_t getChannel();
	void setChannel(uint8_t channel);
	void setPower(uint8_t power, uint8_t gain);

	void scanRSSI(uint16_t *buffer, uint8_t start_channel, uint8_t num_channels);
	uint8_t getRSSI();

	void listen();
	void listen(uint8_t address);
	bool hasData();
	void clearFifo();

	int receive(void *buffer, int maxBufferSize);
	bool send(uint8_t address, const void *buffer, int length, bool wait);

	bool waitData(uint32_t timeout);

	void sleep();
	//void lt8900_powerdown();
	void wakeup();
private:
	void setSelect(bool select);
	bool waitPackage(uint32_t timeout);

	uint16_t readRegister(uint8_t reg);
	uint16_t writeRegister(uint8_t reg, uint16_t value);
	uint16_t writeRegister2(uint8_t reg, uint8_t value1, uint8_t value2);

	Spi *m_spi;
	Lt8900Control *m_lt8900Control;

	uint8_t m_channel = 0;
	uint8_t m_retransmit;
	uint8_t m_rf_address;
};

#endif /* LT8900_H_ */
