/*
 * Lt8900ControlAtmega.h
 *
 *  Created on: 17.01.2017
 *      Author: schnake
 */

#ifndef LT8900CONTROLATMEGA_H_
#define LT8900CONTROLATMEGA_H_

#include "Lt8900Control.h"

#include <stdint.h>

class Lt8900ControlAtmega : public Lt8900Control
{
public:
	Lt8900ControlAtmega();

	void setCs(uint8_t *csPort, uint8_t *csDdr, uint8_t bv);
	void setReset(uint8_t *resetPort, uint8_t *resetDdr, uint8_t bv);
	void setPkt(uint8_t *pktPort, uint8_t *pktDdr, uint8_t *pktPin, uint8_t bv);

private:
	volatile uint8_t *m_csPort, *m_csDdr;
	volatile uint8_t *m_resetPort, *m_resetDdr;
	volatile uint8_t *m_pktPort, *m_pktDdr, *m_pktPin;

	uint8_t m_bvCs, m_bvReset, m_bvPkt;
};

#endif /* LT8900CONTROLATMEGA_H_ */
