/*
 * Pwm.h
 *
 *  Created on: 05.12.2016
 *      Author: schnake
 */

#ifndef PWM_H_
#define PWM_H_

class Pwm
{
public:
	Pwm();

	enum channel : uint8_t {
		R=0,
		G=1,
		B=2
	};

	inline void setDimLevel(uint8_t port, channel channel, uint8_t level, uint8_t dimmer) {
		setFrequency(port, channel, ((uint16_t)level)*63/100, dimmer);
	}
	inline void setDimLevel(uint8_t port, uint8_t r, uint8_t g, uint8_t b, uint8_t dimmer) {
		setFrequency(port, channel::R, ((uint16_t)r)*63/100, dimmer);
		setFrequency(port, channel::G, ((uint16_t)g)*63/100, dimmer);
		setFrequency(port, channel::B, ((uint16_t)b)*63/100, dimmer);
	}

private:
	uint8_t readRawValue(uint8_t level);
	void setFrequency(uint8_t port, channel channel, uint8_t level, uint8_t dimmer);
};

#endif /* PWM_H_ */
