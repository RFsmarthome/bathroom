/*
 * lt8900.cpp
 *
<<<<<<< HEAD
 * Copyright (C) 2015 Rob van der Veer, <rob.c.veer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This version based on the library from Rob van der Veer.
 * The library was modified to support deep sleep an interrupted
 * base wake ups.
 * Registered are based on last datasheet recommendations.
 *
 * Send and listen could now be address based.
=======
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
>>>>>>> e1
 */

#include "lt8900.h"

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>
#include "schnakebus.h"

void Lt8900::setSelect(bool select)
{
	if(select) {
		LT8900_CS_PORT &= ~_BV(LT8900_CS_PIN);
	} else {
		LT8900_CS_PORT |= _BV(LT8900_CS_PIN);
	}
}

Lt8900::Lt8900(uint8_t rf_address, Spi *spi, Lt8900Control *lt8900Control, uint8_t retransmit, uint8_t channel, bool pkt_pullup)
{
	m_rf_address = rf_address;
	m_spi = spi;
	m_retransmit = retransmit;
	m_channel = channel;
	m_lt8900Control = lt8900Control;

	LT8900_CS_DDR |= _BV(LT8900_CS_PIN); // CS as output
	LT8900_RST_DDR |= _BV(LT8900_RST_PIN); // RST as output
	LT8900_PKT_DDR &= ~_BV(LT8900_PKT_PIN); // PKT pin as input

	setSelect(false);
	LT8900_CS_PORT |= _BV(LT8900_CS_PIN); // CS high
	LT8900_RST_PORT |= _BV(LT8900_RST_PIN); // RST high

	if(pkt_pullup) {
		LT8900_PKT_PORT |= _BV(LT8900_PKT_PIN); // PKT Pull up on
	} else {
		LT8900_PKT_PORT &= ~_BV(LT8900_PKT_PIN); // PKT Pull up off
	}

	reset();
}

void Lt8900::reset()
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		LT8900_RST_PORT &= ~_BV(LT8900_RST_PIN);
		_delay_ms(20);
		LT8900_RST_PORT |= _BV(LT8900_RST_PIN);
		_delay_ms(5);
	}
}

bool Lt8900::test()
{
	uint8_t data[3];

	data[0] = 0x80;
	data[1] = 0x00;
	data[2] = 0x00;

	setSelect(true);
	_delay_us(250);

	data[0] = m_spi->transmit(data[0]);
	_delay_us(50);

	data[1] = m_spi->transmit(data[1]);
	_delay_us(50);

	data[2] = m_spi->transmit(data[2]);

	_delay_us(250);
	setSelect(false);

	if(data[0] == 0x01 && data[1] == 0x6f && data[2] == 0xe0) {
		return true;
	}
	return false;
}

uint16_t Lt8900::readRegister(uint8_t reg)
{
	uint8_t data[3];

	data[0] = reg | 0x80;
	data[1] = 0x00;
	data[2] = 0x00;

	setSelect(true);
	_delay_us(250);

	data[0] = m_spi->transmit(data[0]);
	_delay_us(50);

	data[1] = m_spi->transmit(data[1]);
	_delay_us(50);

	data[2] = m_spi->transmit(data[2]);

	_delay_us(250);
	setSelect(false);

	return (data[1] << 8 | data[2]);
}

uint16_t Lt8900::writeRegister(uint8_t reg, uint16_t value)
{
	uint8_t data[3];

	data[0] = reg & 0x7f;
	data[1] = (uint8_t)(value >> 8);
	data[2] = (uint8_t)value & 0xff;

	setSelect(true);
	_delay_us(250);

	data[0] = m_spi->transmit(data[0]);
	_delay_us(50);

	data[1] = m_spi->transmit(data[1]);
	_delay_us(50);

	data[2] = m_spi->transmit(data[2]);

	_delay_us(250);
	setSelect(false);

	return ((uint16_t)data[1]) << 8 | (uint16_t)data[2];
}

uint16_t Lt8900::writeRegister2(uint8_t reg, uint8_t value1, uint8_t value2)
{
	uint8_t data[3];

	data[0] = reg & 0x7f;
	data[1] = value1;
	data[2] = value2;

	setSelect(true);
	_delay_us(250);

	data[0] = m_spi->transmit(data[0]);
	_delay_us(50);

	data[1] = m_spi->transmit(data[1]);
	_delay_us(50);

	data[2] = m_spi->transmit(data[2]);

	_delay_us(250);
	setSelect(false);

	return ((uint16_t)data[1]) << 8 | (uint16_t)data[2];
}

void Lt8900::setSyncWord(uint64_t syncword)
{
	writeRegister(36, syncword & 0xffff);
	writeRegister(37, (syncword >> 16) & 0xffff);
	writeRegister(38, (syncword >> 32) & 0xffff);
	writeRegister(39, (syncword >> 48) & 0xffff);
}

void Lt8900::setAddress(uint8_t address)
{
	uint64_t sync = SYNC_WORD;
	sync ^= ((uint64_t)address << 56 | (uint64_t)address << 48 | (uint64_t)address << 40 | (uint64_t)address << 32
	        | (uint64_t)address << 24 | (uint64_t)address << 16 | (uint64_t)address << 8 | (uint64_t)address);
	setSyncWord(sync);
	_delay_us(20);
}

void Lt8900::setChannel(uint8_t channel)
{
	m_channel = channel;
	writeRegister(7, m_channel & 0x007f);
}

uint8_t Lt8900::getChannel()
{
	return m_channel;
}

void Lt8900::init_hardware()
{
	// Set to recommended values
	/*
	 writeRegister(0, 0x6fef);
	 writeRegister(1, 0x5681);
	 writeRegister(2, 0x6617);
	 writeRegister(4, 0x9cc9);
	 writeRegister(5, 0x6637);
	 writeRegister(7, 0x0030);
	 writeRegister(8, 0x6c90);
	 writeRegister(9, 0x1840);
	 writeRegister(10, 0x7ffd);
	 writeRegister(11, 0x0008);
	 writeRegister(12, 0x0000);
	 writeRegister(13, 0x48bd);
	 writeRegister(22, 0x00ff);
	 writeRegister(23, 0x8005);
	 writeRegister(24, 0x0067);
	 writeRegister(25, 0x1659);
	 writeRegister(26, 0x19e0);
	 writeRegister(27, 0x1200);
	 writeRegister(28, 0x1800);
	 writeRegister(32, 0x1806 | 0b11010000); // FEC 1/3 Interleave
	 writeRegister(33, 0x3ff0);
	 writeRegister(34, 0x3000);
	 writeRegister(35, 0x0080 | ((m_retransmit & 0x0f)<<8));  // times re-transmit (4bit)
	 writeRegister(40, 0x2107);
	 writeRegister(41, 0b1011100000000000) ; // CRC & AUTO_ACK
	 writeRegister(42, 0xfdb0);
	 writeRegister(43, 0x000f);
	 */
	writeRegister(0, 0x6fe0);
	writeRegister(1, 0x5681);
	writeRegister(2, 0x6617);
	writeRegister(4, 0x9cc9);    //why does this differ from powerup (5447)
	writeRegister(5, 0x6637);    //why does this differ from powerup (f000)
	writeRegister(8, 0x6c90);    //power (default 71af) UNDOCUMENTED

	writeRegister(9, 0x1840);
	writeRegister(10, 0x7ffd);   //bit 0: XTAL OSC enable
	writeRegister(11, 0x0008);   //bit 8: Power down RSSI (0=  RSSI operates normal)
	writeRegister(12, 0x0000);
	writeRegister(13, 0x48bd);   //(default 4855)

	writeRegister(22, 0x00ff);
	writeRegister(23, 0x8005);  //bit 2: Calibrate VCO before each Rx/Tx enable
	writeRegister(24, 0x0067);
	writeRegister(25, 0x1659);
	writeRegister(26, 0x19e0);
	writeRegister(27, 0x1300);  //bits 5:0, Crystal Frequency adjust
	writeRegister(28, 0x1800);

	//fedcba9876543210
	writeRegister(32, 0b1001100010010000); //AAABBCCCDDEEFFFG  A preamble length, B, syncword length, c trailer length, d packet type
	//                  E FEC_type, F BRCLK_SEL, G reserved
	//0x5000 = 0101 0000 0000 0000 = preamble 010 (3 bytes), B 10 (48 bits)
	writeRegister(33, 0x3fc7);
	writeRegister(34, 0x2000);  //
	writeRegister(35, m_retransmit << 8);  //POWER mode,  bit 8/9 on = retransmit = 3x (default)

	//lt8900_setSyncWord(SYNC_WORD);

	writeRegister(40, 0x4401);  //max allowed error bits = 0 (01 = 0 error bits)
	writeRegister(41, 0x8000 | 0x4000 | 0x2000 | 0x1000 | (1 << 11));

	writeRegister(42, 0xfdb0);
	writeRegister(43, 0x000f);

	//lt8900_setCurrentControl(15, 0);

	writeRegister(50, 0x0000);  //TXRX_FIFO_REG (FIFO queue)

	writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset

	_delay_us(200);
	writeRegister(7, _BV(8));  //set TX mode.  (TX = bit 8, RX = bit 7, so RX would be 0x0080)
	_delay_us(2);
	writeRegister(7, m_channel);  // Frequency = 2402 + channel
}

void Lt8900::sleep()
{
	writeRegister(7, m_channel);
	writeRegister(35, (1 << 14));
}

/*
 void lt8900_powerdown()
 {
 lt8900_writeRegister(35, lt8900_readRegister(35) | (1<<15));
 RST_PORT &= ~_BV(RST_PIN);
 }
 */

void Lt8900::wakeup()
{
	setSelect(true);
	_delay_us(50);
	setSelect(false);
	_delay_us(50);

	writeRegister(35, m_retransmit << 8);  //POWER mode,  bit 8/9 on = retransmit = 3x (default)
	writeRegister(7, m_channel);
}

void Lt8900::setPower(uint8_t power, uint8_t gain)
{
	writeRegister(9, ((power & 0x0f) << 12) | ((gain & 0x0f) << 7));
}

bool Lt8900::waitPackage(uint32_t timeout)
{
	for(uint16_t t = 0; t < timeout * 5; ++t) {
		_delay_us(200);
		if((LT8900_PKT_PORT & LT8900_PKT_PIN) != 0) {
			return true;
		}
	}
	return false;
}

void Lt8900::scanRSSI(uint16_t *buffer, uint8_t startChannel, uint8_t numChannels)
{
	writeRegister(52, 0x8080);  //flush rx

	//set number of channels to scan.
	writeRegister(42, (readRegister(42) & 0b0000001111111111) | (((numChannels - 1) & 0b111111) << 10));

	//set channel scan offset.
	writeRegister(43, (readRegister(43) & 0b0000000011111111) | ((startChannel & 0b1111111) << 8));

	// start rssi scan
	writeRegister(43, (readRegister(43) & 0b0111111111111111) | (1 << 15));

	waitPackage(15);

	uint8_t pos = 0;
	while(pos < numChannels) {
		uint16_t data = readRegister(50);
		buffer[pos++] = data >> 8;
	}
}

uint8_t Lt8900::getRSSI()
{
	return readRegister(6) >> 10;
}

bool Lt8900::hasData()
{
	if((LT8900_PKT_SPIN & _BV(LT8900_PKT_PIN)) != 0) {
		return true;
	} else {
		return false;
	}
}

void Lt8900::listen()
{
	listen(m_rf_address);
}

void Lt8900::listen(uint8_t address)
{
	setAddress(address);
	writeRegister(7, m_channel & 0x7f); // Set channel and clear rx and tx
	_delay_ms(3);
	writeRegister(52, 0x8080); // Clear fifo bit
	writeRegister(7, (m_channel & 0x7f) | (1 << 7)); // Enable RX bit
}

int Lt8900::receive(void *buffer, int maxBufferSize)
{
	uint16_t status = readRegister(48);
	// Check CRC error
	if((status & 0x8000) == 0) {
		uint16_t data = readRegister(50);
		uint8_t length = data >> 8;
		if(length > maxBufferSize) {
			// Buffer to small
			return -2;
		}

		uint8_t pos = 0;
		((uint8_t*)buffer)[pos++] = data & 0xff;
		while(pos < length) {
			data = readRegister(50);
			((uint8_t*)buffer)[pos++] = data >> 8;
			if(pos < length) {
				((uint8_t*)buffer)[pos++] = data & 0xff;
			}
		}

		return length;
	} else {
		// CRC error
		return -1;
	}
}

bool Lt8900::send(uint8_t address, const void *buffer, int length, bool wait)
{
	if(length < 1 || length > 255) {
		return false;
	}

	setAddress(address);

	writeRegister(7, 0);
	writeRegister(52, 0x8000);

	uint8_t pos = 0;
	uint8_t msb, lsb;
	writeRegister2(50, (uint8_t)length, ((uint8_t*)buffer)[pos++]);
	while(pos < length) {
		msb = ((uint8_t*)buffer)[pos++];
		lsb = ((uint8_t*)buffer)[pos++];
		writeRegister2(50, msb, lsb);
	}

	writeRegister(7, (m_channel & 0x7f) | (1 << 8));
	if(wait) {
		while((LT8900_PKT_SPIN & _BV(LT8900_PKT_PIN)) == 0);
	}
	return true;
}

bool Lt8900::waitData(uint32_t timeout)
{
	if(timeout == 0) { // Compare and branch cause more sleep possible
		while(!hasData())
			;
		return true;
	} else {
		unsigned int count = 0; // Timeout wait for data

		while(!hasData() && count++ < timeout) {
			_delay_ms(1);
		}
		if(count >= timeout) {
			return false;
		}
		return true;
	}
}

extern void Lt8900::clearFifo()
{
	writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset
}
