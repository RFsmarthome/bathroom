/*
 * SoftSpi.cpp
 *
 *  Created on: 14.12.2016
 *      Author: schnake
 */

#include "SoftSpi.h"

#include <avr/io.h>
#include <util/atomic.h>

SoftSpi::SoftSpi(volatile uint8_t &ddr, volatile uint8_t &port, volatile uint8_t &pin, uint8_t clk, uint8_t mosi, uint8_t miso)
{
	m_ddr = &ddr;
	m_port = &port;
	m_pin = &pin;

	m_clk = _BV(clk);
	m_mosi =_BV( mosi);
	m_miso = _BV(miso);

	*m_ddr |= m_clk | m_mosi; // Config for output
	*m_ddr &= ~m_miso; // Config for input
}

uint8_t SoftSpi::transmit(uint8_t out)
{
	uint8_t in;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		for(uint8_t i=8; i; i--) {
			clk_low();
			if(out & 0x80) { // Set MOSI
				mosi_high();
			} else {
				mosi_low();
			}
			out <<= 1;
			clk_high(); // CPHA1!

			in <<= 1;
			if(*m_pin & m_miso) { // Check MISO
				in |= 0b00000001;
			} else {
				in &= 0b11111110;
			}
		}
		clk_low(); // Thats all!
	}

	return in;
}

void SoftSpi::wait()
{

}
