/*
 * Spi.h
 *
 *  Created on: 15.12.2016
 *      Author: schnake
 */

#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>

class Spi
{
public:
	virtual uint8_t transmit(uint8_t out) = 0;
	virtual void wait() = 0;

protected:
	~Spi() {};
};

#endif /* SPI_H_ */
