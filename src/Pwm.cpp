/*
 * Pwm.cpp
 *
 *  Created on: 05.12.2016
 *      Author: schnake
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "Pwm.h"

const uint16_t pwmtable[64] PROGMEM = {
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25,
	28, 30, 33, 36, 39, 42, 46, 49, 53, 56, 60, 64, 68, 72, 77, 81, 86, 90,
	95, 100, 105, 110, 116, 121, 127, 132, 138, 144, 150, 156, 163, 169,
	176, 182, 189, 196, 203, 210, 218, 225, 233, 240, 248, 255
};


Pwm::Pwm()
{
	DDRD |= _BV(PD5) | _BV(PD6) | _BV(PD3);
	PORTD &= ~(_BV(PD5) | _BV(PD6) | _BV(PD3));

	DDRB |= _BV(PB1) | _BV(PB2) | _BV(PB3);
	PORTB &= ~(_BV(PB1) | _BV(PB2) | _BV(PB3));

	TCCR0A = _BV(COM0A1) | _BV(COM0A0) | _BV(COM0B1)| _BV(COM0B0) | _BV(WGM01) | _BV(WGM00); // FastPWM, Inverting Mode
	TCCR0B = _BV(CS00); // No Prescaler

	TCCR1A = _BV(COM1A1) | _BV(COM1A0) | _BV(COM1B1) | _BV(COM1B0) |_BV(WGM00); // FastPWM 8Bit, Inverting Mode
	TCCR1B = _BV(WGM12) | _BV(CS10); // No Prescaler

	TCCR2A = _BV(COM2A1) | _BV(COM2A0) | _BV(COM2B1)| _BV(COM2B0) | _BV(WGM01) | _BV(WGM00);
	TCCR2B = _BV(CS20); // No Prescaler

	OCR0A = 0;
	OCR0B = 0;

	OCR1A = 0;
	OCR1B = 0;

	OCR2A = 0;
	OCR2B = 0;
}

uint8_t Pwm::readRawValue(uint8_t level)
{
	return pgm_read_byte(&pwmtable[level & 0x3f]);
}

void Pwm::setFrequency(uint8_t port, channel channel, uint8_t level, uint8_t dimmer)
{
	uint8_t tmp = ((uint16_t)readRawValue(level))*dimmer/100;
	if(port==0) {
		switch(channel) {
			case R:
				OCR0A = tmp;
				break;
			case G:
				OCR1A = tmp;
				break;
			case B:
				OCR0B = tmp;
				break;
		}
	} else if(port==1) {
		switch(channel) {
			case R:
				OCR1B = tmp;
				break;
			case G:
				OCR2A = tmp;
				break;
			case B:
				OCR2B = tmp;
				break;
		}
	}
}
