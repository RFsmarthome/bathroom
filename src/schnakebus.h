/*
 * schnakebus.h
 *
 * Definition of the air protocol
 *
 * Copyright(c) 2016 by Marcus Schneider <schnake24@gmail.com>
 * GPLv3
 *
 */

#ifndef SCHNAKEBUS_H_
#define SCHNAKEBUS_H_

#define VERSION1 0
#define VERSION2 1
#define VERSION3 2
#define SB_VERSION VERSION3

#define ADDR_MASTER 0

#include <stdint.h>

#define TYPE_BMP180 0
#define TYPE_DHT22 1
#define TYPE_RGB 2
#define TYPE_DIMMER 3
#define TYPE_BATTERY 4
#define TYPE_ON_OFF 6
#define TYPE_WRGB1 7
#define TYPE_WRGB2 8
#define TYPE_TEMPERATURE 9
#define TYPE_DELAY 10
#define TYPE_FADE 11

struct __attribute__((__packed__)) bmp180_t
{
	int32_t pressure;
	int16_t temperature;
};

struct __attribute__((__packed__)) dht22_t
{
	uint16_t humanity;
	int16_t temperature;
};

struct __attribute__((__packed__)) rgb_t
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct __attribute__((__packed__)) onoff_t
{
	uint8_t onoff;
};

struct __attribute__((__packed__)) dimmer_t
{
	uint8_t dimmer;
};

struct __attribute__((__packed__)) battery_t
{
	uint16_t battery;
	uint16_t battery_raw;
};

struct __attribute__((__packed__)) blink_program_t
{
	uint16_t mode;
};

struct __attribute__((__packed__)) wrgb1_t
{
	uint8_t channel;

	uint8_t w1;
	uint8_t r1;
	uint8_t g1;
	uint8_t b1;
};

struct __attribute__((__packed__)) wrgb2_t
{
	uint8_t w1;
	uint8_t r1;
	uint8_t g1;
	uint8_t b1;

	uint8_t w2;
	uint8_t r2;
	uint8_t g2;
	uint8_t b2;
};

struct __attribute__((__packed__)) temperature_t
{
	int16_t temperature;
};

struct __attribute__((__packed__)) delay_t
{
	uint8_t channel;
	int16_t delay;
};

struct __attribute__((__packed__)) fade_t
{
	uint8_t channel;
	int16_t fade;
};

struct __attribute__((__packed__)) buspacket_t
{
	uint8_t version;
	uint8_t type;

	uint8_t source;
	uint16_t packet;

	uint8_t ack :1;
	uint8_t req_ack :1;

	union
	{
		struct bmp180_t bmp180;
		struct dht22_t dht22;
		struct rgb_t rgb;
		struct onoff_t onoff;
		struct dimmer_t dimmer;
		struct battery_t battery;
		struct wrgb1_t wrgb1;
		struct wrgb2_t wrgb2;
		struct temperature_t temperature;
		struct delay_t delay;
		struct fade_t fade;
	};
	uint8_t dummy;
	uint8_t crc8;
};

#endif /* SCHNAKEBUS_H_ */
