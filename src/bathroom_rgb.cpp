
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>

#include <avr/power.h>

#include "schnakebus.h"
#include "Pwm.h"
#include "SoftSpi.h"
#include "Lt8900ControlAtmega.h"
#include "lt8900.h"
#include "crc8.h"

#define SPI_DDR DDRC
#define SPI_PORT PORTC
#define SPI_PIN PINC
#define SPI_SS _BV(2)
#define SPI_CLK _BV(3)
#define SPI_MOSI _BV(4)
#define SPI_MISO _BV(5)

#define PIR_DDR DDRD
#define PIR_PORT PORTD
#define PIR_PIN PIND
#define PIR_PAD PD2

#define DEBUG_DDR DDRD
#define DEBUG_PORT PORTD
#define DEBUG_PIN PIND
#define DEBUG_PAD PD4

#define UP 1
#define NONE 0
#define DOWN -1

#define TIMEOUT_SET 50
#define TIMEOUT_MOVE 300
#define DIM_LEVELS 20

#ifndef _RF_ADDRESS
uint8_t rf_address EEMEM = 5;
#else
uint8_t rf_address EEMEM = _RF_ADDRESS;
#endif

uint8_t g_rgb[6] EEMEM = {100, 100, 100, 100, 100, 100};


void initPir()
{
	PIR_DDR &= ~_BV(PIR_PAD);
	PIR_PORT &= ~_BV(PIR_PAD);
}

void initDebug()
{
	DEBUG_DDR |= _BV(DEBUG_PAD);
	DEBUG_PORT &= ~_BV(DEBUG_PAD);
}

void debugOn()
{
	DEBUG_PORT |= _BV(DEBUG_PAD);
}
void debugOff()
{
	DEBUG_PORT &= ~_BV(DEBUG_PAD);
}


int main()
{
	clock_prescale_set(clock_div_1);

	uint8_t rgb[6];
	struct buspacket_t packet;
	int16_t timer[2];
	int8_t on_off = 0;
	int8_t dim_level[2];
	int8_t dim_direction[2];

	cli();

	initDebug();
	initPir();

	Pwm pwm; // Init PWM and set all to off
	for(uint8_t a=0; a<6; ++a) {
		rgb[a] = eeprom_read_byte(&g_rgb[a]);
	}
	for(uint8_t a=0; a<2; ++a) {
		//pwm.setDimLevel(a, rgb[a*3+0], rgb[a*3+1], rgb[a*3+2], 100);
		pwm.setDimLevel(a, 0, 0, 0, 0);
		timer[a] = TIMEOUT_SET; // 5 Seconds
		//trigger[a] = 1;
		dim_direction[a] = UP;
		dim_level[a] = 0;
	}



	Crc8 crc(0x5d); // Init CRC8 with polynom

	SoftSpi spi(DDRC, PORTC, PINC, 3, 4, 5); // Init SoftSPI and LT8900
	Lt8900ControlAtmega lt8900Control;
	Lt8900 lt8900(eeprom_read_byte(&rf_address), &spi, &lt8900Control, 8, 120, false);

	lt8900.reset();
	lt8900.init_hardware();
	lt8900.setPower(15, 7);

	lt8900.setAddress(eeprom_read_byte(&rf_address));
	if(!lt8900.test()) {
		// LT8900 Test failed!
		for(;;) {
			// Blink forever
			pwm.setDimLevel(0, Pwm::channel::R, 0, 0);
			pwm.setDimLevel(0, Pwm::channel::G, 0, 0);
			pwm.setDimLevel(0, Pwm::channel::B, 0, 0);
			pwm.setDimLevel(1, Pwm::channel::R, 100, 100);
			pwm.setDimLevel(1, Pwm::channel::G, 100, 100);
			pwm.setDimLevel(1, Pwm::channel::B, 100, 100);
			_delay_ms(1000);
			pwm.setDimLevel(0, Pwm::channel::R, 100, 100);
			pwm.setDimLevel(0, Pwm::channel::G, 100, 100);
			pwm.setDimLevel(0, Pwm::channel::B, 100, 100);
			pwm.setDimLevel(1, Pwm::channel::R, 0, 0);
			pwm.setDimLevel(1, Pwm::channel::G, 0, 0);
			pwm.setDimLevel(1, Pwm::channel::B, 0, 0);
			_delay_ms(1000);
		}
	}

	for(;;) {
		lt8900.listen();
		if(lt8900.waitData(100)) {
			if(lt8900.hasData()) {
				uint8_t length = lt8900.receive(&packet, sizeof(packet));
				if(crc.message((uint8_t*)&packet, length)==0) {
					if(packet.type==TYPE_WRGB1) {
						if(packet.wrgb1.channel<2) {
							rgb[packet.wrgb1.channel*3+0] = packet.wrgb1.r1;
							eeprom_busy_wait();
							eeprom_update_byte(&g_rgb[packet.wrgb1.channel*3+0], packet.wrgb1.r1);
							rgb[packet.wrgb1.channel*3+1] = packet.wrgb1.g1;
							eeprom_busy_wait();
							eeprom_update_byte(&g_rgb[packet.wrgb1.channel*3+1], packet.wrgb1.g1);
							rgb[packet.wrgb1.channel*3+2] = packet.wrgb1.b1;
							eeprom_busy_wait();
							eeprom_update_byte(&g_rgb[packet.wrgb1.channel*3+2], packet.wrgb1.b1);

							if(timer[packet.wrgb1.channel]<50) {
								timer[packet.wrgb1.channel] = TIMEOUT_SET; // 5 Seconds (cause only for settings)
							}
							dim_direction[packet.wrgb1.channel] = UP;
						}
					} else if(packet.type==TYPE_ON_OFF) {
						if(packet.onoff.onoff==1) {
							on_off = 1;
							for(int8_t a=0; a<2; ++a) {
								timer[a] = TIMEOUT_SET;
								dim_direction[a] = UP;
							}
						} else {
							on_off = 0;
						}
					}
				}
			}
		}

		if((PIR_PIN & _BV(PIR_PAD))!=0) { // check PIR
			debugOn();
			// Set timeouts
			for(uint8_t channel=0; channel<2; ++channel) {
				timer[channel] = TIMEOUT_MOVE; // 15 secs
				dim_direction[channel] = UP;
			}
		} else {
			debugOff();
		}

		// Light control
		for(uint8_t channel=0; channel<2; ++channel) {
			if(timer[channel]>0 && on_off==0) {
				--timer[channel];
			} else if(timer[channel]==0) {
				dim_direction[channel] = DOWN;
				timer[channel] = -1;
			}



			if(dim_direction[channel] != NONE) {
				if((dim_direction[channel] == UP) && (dim_level[channel] < DIM_LEVELS)) {
					++dim_level[channel];
				}
				if((dim_direction[channel] == DOWN) && (dim_level[channel] > 0)) {
					--dim_level[channel];
				}

				pwm.setDimLevel(channel, rgb[channel*3+0], rgb[channel*3+1], rgb[channel*3+2], dim_level[channel]*(100/DIM_LEVELS));

				if((dim_level[channel] >= DIM_LEVELS) || (dim_level[channel] <= 0)) {
					dim_direction[channel] = NONE;
				}
			}
		}

	}
}
