/*
 * Crc8.h
 *
 *  Created on: 02.11.2015
 *      Author: schnake
 */

#ifndef _CRC8_H_
#define _CRC8_H_

#include <stdint.h>

class Crc8
{
public:
	Crc8(uint8_t polynom);

	uint8_t message(const uint8_t *msg, uint8_t len);
private:
	uint8_t m_polynom;
};

#endif /* CRC8_H_ */
