/*
 * SoftSpi.h
 *
 *  Created on: 14.12.2016
 *      Author: schnake
 */

#ifndef SOFTSPI_H_
#define SOFTSPI_H_

#include <stdint.h>
#include "Spi.h"

class SoftSpi : public Spi
{
public:
	SoftSpi(volatile uint8_t &ddr, volatile uint8_t &port, volatile uint8_t &pin, uint8_t clk, uint8_t mosi, uint8_t miso);

	inline void clk_low() { *m_port &= ~m_clk; }
	inline void clk_high() { *m_port |= m_clk; }
	inline void mosi_low() { *m_port &= ~m_mosi; }
	inline void mosi_high() { *m_port |= m_mosi; }

	uint8_t transmit(uint8_t out);
	void wait();

private:
	volatile uint8_t *m_ddr;
	volatile uint8_t *m_port;
	volatile uint8_t *m_pin;

	uint8_t m_clk;
	uint8_t m_mosi;
	uint8_t m_miso;
};

#endif /* SOFTSPI_H_ */
